package com.fix305.connector.exposed

import com.fix305.common.config.Config
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database

class DbConnector private constructor(
    private val config: DbConfig
) {
    val db: Database by lazy {
        val hikariConfig = HikariConfig().apply {
            jdbcUrl = config.url
            maximumPoolSize = config.maxPoolSize
            transactionIsolation = TRANSACTION_ISOLATION
            username = config.username
            password = config.password

            validate()
        }

        val source = HikariDataSource(hikariConfig)

        Database.connect(source)
    }

    companion object {
        private const val TRANSACTION_ISOLATION = "TRANSACTION_REPEATABLE_READ"

        fun create(config: Config): DbConnector {
            val dbConfig = DbConfig.create(config)

            return DbConnector(dbConfig)
        }
    }
}
