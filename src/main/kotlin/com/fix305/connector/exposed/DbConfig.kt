package com.fix305.connector.exposed

import com.fix305.common.config.Config
import com.fix305.common.config.get

internal data class DbConfig (
    val url: String,
    val username: String,
    val password: String,
    val maxPoolSize: Int
) {
    companion object {
        private const val DEFAULT_SCHEME = "postgresql"
        private const val DEFAULT_MAX_POOL_SIZE = 5
        private const val DEFAULT_HOST = "127.0.0.1"
        private const val DEFAULT_PORT = 5432
        private const val DEFAULT_DATABASE = "notifier_local"
        private const val DEFAULT_USER = "admin"
        private const val DEFAULT_PASSWORD = "password"

        fun create(config: Config): DbConfig {
            val pgConfig = config.getConfig("postgres")

            val scheme = pgConfig["scheme"] ?: DEFAULT_SCHEME
            val host = pgConfig["host"] ?: DEFAULT_HOST
            val port = pgConfig["port"]?.toInt() ?: DEFAULT_PORT
            val database = pgConfig["database"] ?: DEFAULT_DATABASE
            val user = pgConfig["user"] ?: DEFAULT_USER
            val password = pgConfig["password"] ?: DEFAULT_PASSWORD

            val connection = "$scheme://$host:$port/$database"

            return DbConfig(
                url = "jdbc:$connection",
                username = user,
                password = password,
                maxPoolSize = pgConfig["maxPoolSize"]?.toIntOrNull() ?: DEFAULT_MAX_POOL_SIZE,
            )
        }
    }
}
