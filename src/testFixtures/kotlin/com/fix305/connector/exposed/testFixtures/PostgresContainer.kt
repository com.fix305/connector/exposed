package com.fix305.connector.exposed.testFixtures

import com.fix305.common.config.Config
import com.fix305.common.config.ConfigMap
import com.fix305.common.config.ConfigProvider
import com.fix305.common.config.plus
import com.fix305.connector.exposed.DbConnector
import org.testcontainers.containers.GenericContainer

object PostgresContainer {
    private var config: Config = ConfigProvider.Default

    val db by lazy {
        val container = GenericContainer(BASE_IMAGE)
            .withExposedPorts(PORT)
            .withEnv("POSTGRES_DB", POSTGRES_DB)
            .withEnv("POSTGRES_USER", POSTGRES_USER)
            .withEnv("POSTGRES_PASSWORD", POSTGRES_PASSWORD)

        container.start()

        config += ConfigMap(
            "postgres.host" to container.host,
            "postgres.port" to container.getMappedPort(PORT).toString(),
            "postgres.database" to POSTGRES_DB,
            "postgres.user" to POSTGRES_USER,
            "postgres.password" to POSTGRES_PASSWORD,
        )

        DbConnector.create(config = config)
    }

    private const val BASE_IMAGE = "postgres:14.2"
    private const val PORT = 5432
    private const val POSTGRES_DB = "test_db"
    private const val POSTGRES_USER = "admin"
    private const val POSTGRES_PASSWORD = "password"
}
