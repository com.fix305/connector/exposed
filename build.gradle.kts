plugins {
    kotlin("jvm") version "1.9.0"
    id("maven-publish")
    id("java-test-fixtures")

    java
}

group = "com.fix305.connector"

repositories {
    mavenCentral()
    maven("https://repo.fix305.com")
}

dependencies {
    implementation("com.fix305.common:config:1.0.1")

    api("org.jetbrains.exposed:exposed-core:0.42.0")
    api("org.jetbrains.exposed:exposed-java-time:0.42.0")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.42.0")
    implementation("org.postgresql:postgresql:42.5.4")
    implementation("com.zaxxer:HikariCP:5.0.1")

    testFixturesImplementation("com.fix305.common:config:1.0.1")
    testFixturesImplementation("org.testcontainers:testcontainers:1.18.3")
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")
            name = "GitLab"
            version = findProperty("version") ?: "undefined"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }

    withJavadocJar()
    withSourcesJar()
}